def log(message, level = 1, end='\n'):
    """
    Adds message to the log
    :param message: Message to be logged
    :param level: Error level: 0 = Debug, 1 = Info, 2 = Warning, 3 = Error
    :return: None
    """
    print(message, end=end)
    return None