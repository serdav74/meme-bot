import config
from logger import log
import threading

PROCESSING_INTERVAL = 0.1 # 10 messages per second max
QUEUE = []


def process_queue():
    valid = False
    try:
        if len(QUEUE) > 0:
            r = QUEUE.pop(0)
            if r.is_valid:
                valid = True
                config.vk_client.messages.send(
                    message=r.Text,
                    peer_id=r.Peer_ID,
                    attachment=','.join(r.Attachments) if r.Attachments is not None else ''
                )
                log('>>> {}...'.format(r.Text[:8]))
            else:
                log('xxx Popped invalid response from queue!\n'+str(r), 2)
    except Exception as error:
        log(str(error), 3)
    else:
        if valid: config.messages_sent += 1
    if not config.die:
        threading.Timer(PROCESSING_INTERVAL, process_queue).start()