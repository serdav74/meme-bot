class Response:
    Peer_ID = -1
    User_ID = -1
    Text = ''
    Attachments = None

    def __init__(self, peer_id, text = '', attachments = None):
        self.Peer_ID = peer_id
        self.Text = text
        self.Text.replace('&lt;', '<').replace('&gt;', '>').replace('&quot;', '"')
        self.Attachments = attachments

    @property
    def is_valid(self):
        return (self.Peer_ID != -1) and ((self.Attachments is not None) or (self.Text != ''))

    def __str__(self):
        return "Response to {}:\n{}".format(self.Peer_ID, self.Text)