class Message:
    ID = -1
    Peer_ID = -1
    User_ID = -1
    Text = ""
    Args = None
    Attachments = None

    def __init__(self, peer_id, user_id, text, attachments, message_id):
        self.Peer_ID = peer_id
        self.User_ID = user_id
        self.Text = text.replace('&lt;', '<').replace('&gt;', '>').replace('&quot;', '"')
        self.Attachments = attachments
        self.Args = self.Text.split()
        self.ID = message_id