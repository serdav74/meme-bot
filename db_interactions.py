import sqlite3

DB_FILENAME = 'db/memes.sqlite'

connection = None


def connect():
    global connection
    connection = sqlite3.connect(DB_FILENAME)
    init_tables()


def disconnect():
    connection.close()


def init_tables():
    queries = [
        'CREATE TABLE IF NOT EXISTS `names` ('
        '`uid` INTEGER NOT NULL DEFAULT -1 UNIQUE, '
        '`first_name` TEXT NOT NULL DEFAULT \'John\','
        '`last_name` TEXT NOT NULL DEFAULT \'Doe\''
        ');',

        'CREATE TABLE IF NOT EXISTS `global-memes` ('
        '`id` TEXT NOT NULL DEFAULT \'meme\' UNIQUE, '
        '`text` TEXT NOT NULL DEFAULT \'Your generic meme\','
        '`attachments` TEXT NOT NULL DEFAULT \'\''
        ');',

        'CREATE TABLE IF NOT EXISTS `memes` ('
        '`uid` INTEGER NOT NULL DEFAULT -1,'
        '`id` TEXT NOT NULL DEFAULT \'meme\', '
        '`text` TEXT NOT NULL DEFAULT \'Your generic meme\','
        '`attachments` TEXT NOT NULL DEFAULT \'\''
        ');',
    ]

    global connection
    cur = connection.cursor()
    for q in queries:
        cur.execute(q)


def init_user(uid):
    pass


def get_user_data(uid):
    cur = connection.cursor()
    cur.execute('SELECT * FROM `names` WHERE uid={} LIMIT 1'.format(uid))
    return cur.fetchone()


def set_user_data(uid, first_name, last_name):
    cur = connection.cursor()
    cur.execute('INSERT OR REPLACE INTO `names` VALUES (:uid, :fn, :ln)', {'uid': uid, 'fn': first_name, 'ln':last_name})
    connection.commit()


def is_meme_exists(id, uid = -1):
    """
    Checks whether meme with given id exists in database.
    :param id: Meme id (string)
    :param uid: VK user ID. If not given, function checks only for global memes
    :return Boolean value
    """
    cur = connection.cursor()
    if uid != -1:
        cur.execute('SELECT * FROM `memes` WHERE uid=? and id=?', (uid, id))
        return len(cur.fetchall()) > 0
    else:
        cur.execute('SELECT * FROM `global-memes` WHERE id=?', (id,))
        return len(cur.fetchall()) > 0


def add_meme(uid, id, text, attachments):
    """
    Adds meme to the database.
    :param uid: VK user ID
    :param id: Meme id (string)
    :param text: Response text
    :param attachments: Response attachments (list)
    """
    cur = connection.cursor()
    if is_meme_exists(id, uid=uid):
        query = 'UPDATE OR REPLACE `memes` SET `text`=:t,`attachments`=:a WHERE `uid`=:u AND `id`=:i'
    else:
        query = 'INSERT INTO `memes` VALUES (:u, :i, :t, :a)'

    cur.execute(query, {'u': uid, 'i':id, 't':text, 'a':','.join(attachments)})
    connection.commit()


def get_meme(uid, id):
    """
    Retrieves specified meme from database. Throws KeyError if meme is not found.
    :param uid: VK user ID
    :param id: Meme id (string)
    :return: Tuple of text (string) and attachments (list)
    """

    cur = connection.cursor()
    if is_meme_exists(id, uid=uid):
        query = 'SELECT text,attachments FROM `memes` WHERE uid=? AND id=?'
        cur.execute(query, (uid, id))
    elif is_meme_exists(id):
        query = 'SELECT text,attachments FROM `global-memes` WHERE id=?'
        cur.execute(query, (id,))
    else:
        raise KeyError

    data = cur.fetchone()
    return data[0], data[1].split(',')