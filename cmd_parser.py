import msg_queue, db_interactions
from classes.response import Response
import config

def steal_attachments(msg_id):
    reattachments = []

    msg = config.vk_client.messages.getById(message_ids=msg_id)

    # if __debug__:
    #     import pprint
    #     pprint.pprint(msg)

    if 'attachments' in msg['items'][0].keys():
        attachments = msg['items'][0]['attachments']

        for attach in attachments:
            types = ['photo', 'video', 'doc']

            for type_ in types:
                if type_ in attach.keys():
                    template = '{}{}_{}_{}'
                    owner_id = attach[type_]['owner_id']
                    attach_id = attach[type_]['id']
                    access_key = attach[type_]['access_key']
                    reattachments.append(template.format(type_, owner_id, attach_id, access_key))
    return reattachments

def queue(response) -> None:
    msg_queue.QUEUE.append(response)


def parse(msg) -> None:
    if len(msg.Text) == 0:
        return

    if msg.Text[0] == '.':

        from datetime import datetime
        if msg.User_ID in config.cooldowns.keys():
            if (datetime.now() - config.cooldowns[msg.User_ID]).total_seconds() < config.COOLDOWN:
                return
        config.cooldowns[msg.User_ID] = datetime.now()

        cmd_id = msg.Args[0][1:]

        if cmd_id in cmds.keys():
            cmd = cmds[cmd_id]
            cmd(msg)
        else:
            memes(cmd_id, msg)
    else:
        # queue(Response(msg.Peer_ID, msg.Text))
        pass


def memes(id, msg):
    if len(msg.Args)>1 or len([a for a in msg.Attachments.keys() if ('from' not in a)]) > 0:
        meme_text = ' '.join(msg.Args[1:])

        attachs = steal_attachments(msg.ID)

        db_interactions.add_meme(msg.User_ID, id, meme_text, attachs)

        queue(Response(msg.Peer_ID, 'Мемс добавлен. Вызов: .{}'.format(id)))


    else:
        try:
            meme = db_interactions.get_meme(msg.User_ID, id)
        except KeyError:
            queue(Response(msg.Peer_ID, 'Такого мемса в базе нет. Добавь его!'))
            return
        else:
            queue(Response(peer_id=msg.Peer_ID, text=meme[0], attachments=meme[1]))
            return


def help_cmd(msg):
    with open('help.txt') as f:
        help_msg = f.read()
    queue(Response(msg.Peer_ID, help_msg))


def status(msg):
    import platform, datetime, config

    def printNiceTimeDelta(delay):
        template = '{} days, {:02d}:{:02d}:{:02d}'
        return template.format(delay.days, delay.seconds // 3600, delay.seconds // 60 % 60, delay.seconds)

    txt = 'Serjio\'s meme bot\nRunning on: \n{0}\n\nUptime: {1}\nMessages sent: {2}\nValue 1: {3}\nValue 2: {4}'.format(
        platform.platform(),
        printNiceTimeDelta(datetime.datetime.now() - config.launch_time), config.messages_sent, config.vk_client is None, len(config.cooldowns)
    )

    queue(Response(msg.Peer_ID, txt))


def yeah_boi(msg):
    return queue(Response(msg.Peer_ID, 'yeah bo'+'i'*100))


def resend(msg):
    if len(msg.Args) > 0:
        meme_text = ' '.join(msg.Args[1:])
        attachments = steal_attachments(msg.ID)
        print(attachments)
        queue(Response(msg.Peer_ID, meme_text, attachments))

cmds = {
    'help': help_cmd,
    'yeah_boi': yeah_boi,
    'status': status,
    'resend': resend,
}