# importing necessary stuff
import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
import threading

# for handling exceptions:
import vk_api.exceptions
import socket, requests

# importing internal stuff
import config, db_interactions, msg_queue, cmd_parser
from logger import log
from classes.message import Message


def main():
    import datetime
    config.launch_time = datetime.datetime.now()

    log('Authorizing...', end='')
    config.vk_session = vk_api.VkApi(token=config.TOKEN)
    config.vk_client = config.vk_session.get_api()
    log(' done.')

    db_interactions.connect()
    log('Connected to database.')

    longpoll = VkLongPoll(config.vk_session, wait=20)
    log('Longpoll ready.')

    # start queue processing
    msg_queue.process_queue()

    log('Started listening...\n════════════════')

    for event in longpoll.listen():
        if event.to_me and event.type == VkEventType.MESSAGE_NEW:
            d = db_interactions.get_user_data(event.user_id)

            # if there is no data about this user, initialize it
            if d is None:
                result = config.vk_client.users.get(user_ids=event.user_id)[0]
                db_interactions.set_user_data(event.user_id, result['first_name'], result['last_name'])
                db_interactions.init_user(event.user_id)
                d = (event.user_id, result['first_name'], result['last_name'])

            msg = Message(event.peer_id, event.user_id, event.text, event.attachments, event.raw[1])

            log('<<< {} {}.: {}...'.format(d[1], d[2][0], msg.Text[:12]))
            cmd_parser.parse(msg)


if __name__ == '__main__':
    while not config.die:
        try:
            main()
        except (socket.timeout, requests.ConnectionError, requests.ConnectTimeout, requests.HTTPError, requests.ReadTimeout, requests.Timeout) as error:
            log(str(error), 3)
            config.die = True

            db_interactions.disconnect()

            import time
            time.sleep(10)
            config.die = False
            pass
        except KeyboardInterrupt:
            config.die = True

    log('Shutting down...')
    db_interactions.disconnect()
    import time
    time.sleep(1)
